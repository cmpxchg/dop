#include <FL/Fl.H>
#include <FL/Fl_Scroll.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Shared_Image.H>
#include <FL/x.H>
#include <string>
#include <vector>
#include <sstream>
#include "recs.h"
#include "pal.h"
#include "raytadap.h"

extern int fsi;
extern int fsi2;

class PlotArea : public Fl_Scroll {
  public:
    PlotArea(int x, int y, int w, int h, palette* p, const char* t);
    ~PlotArea() { fl_delete_offscreen(offsc); }
    void loadRecs(std::string fname);
    void setPal(palette* p) { pal=p; }
    int handle(int e);
    void draw(void);
    void resize(int, int, int, int);
  private:
    Fl_Offscreen offsc;
    palette* pal;
    bool draggin;
    bool needsrecalc;
    bool firstcalc;
    std::string draggedr;
    reccoll recs;
    double pixpx, pixpy;
    double xmin, ymin, xmax, ymax;
    int xoff, yoff;
    double scale;
    void recalc();
    void met2pix(Eigen::Vector3d, int&, int&);
    void pix2met(int, int, Eigen::Vector2d&);
};


