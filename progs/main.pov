#version 3.7;

#declare micang = 72;
#declare mode = 0;    // 0: Ultrasound, 1: Visible light

#if(mode)
camera {
   location <-.7,1.7,0.5>
   look_at <7,.7,4.5>
}
#else
#include "camera.inc"
#end

#if(mode)
  light_source {
    <6,2.4,3.36>
    color rgb 1
  }
#else
#include "spotlights.inc"
#end


union {  // Saeulen
  box { <2.03,0,.36>,<2.33,3.1,.66> }
  box { <2.03,0,5.79>,<2.33,3.1,6.09> }
  box { <7.45,0,.36>,<7.75,3.1,.66> }
  box { <7.45,0,5.79>,<7.75,3.1,6.09> }
   
#if(mode)
  texture{
     pigment{color rgb 1.}
     finish{
        diffuse .9
     }
  }
#else
  texture{
     pigment{color rgb 0.}
     finish{
        ambient 0.0
        diffuse 1.0
     }
  }
#end
}

union { // Treppe
   box { <2.91, 0, 5.73>, <4.73, .2, 7.37> }
   box { <2.91, 0.2, 6.01>, <4.73, .4, 7.37> }
   box { <2.91, 0.4, 6.29>, <4.73, .6, 7.37> }
   #if(mode)
     texture { pigment { color rgb <1,1,.8> } }
   #else
     texture{
       pigment{color rgb 0.}
       finish{
          ambient 0.0
          diffuse 1.0
       }
     }
  #end
}



union {  // Raum
  box{ <1.78,0.,-.1>,<11.17,3.1,0.> }
  box{ <11.07,0.,0.>,<11.17,3.1,2.38> }
  box{ <11.07,0.,2.38>,<13.1,3.1,2.48> }
  box{ <13., 0., 2.48>,<13.1,3.1,7.47> }
  box{ <-0.1,0,7.37>,<13.1,.834,7.57> }
  box{ <-0.1,.438,7.47>,<13.1,3.1,7.57> }   // Fensterfront
  box{ <-0.1, 0, 1.79>,<0., 3.1, 7.47> }  
  box{ <-.05,0,-2>,<0.05,3.1,2> rotate y*45 translate <-1.4496,0,.4111> }
  box{ <-0.09, 2.64, 5.79>, <13.1, 3.13, 7.56> } // Decke
  box{ <11.07, 2.64, 2.48>, <13.1, 3.13, 5.80> }
  box{ <-.1, 3.04, -.1>,<11.17, 3.14, 5.81> }
  box{ <7.45, 2.74,.65>, <7.75, 3.05, 5.80> }
  box{ <-.8485, 2.64, -.8485>, <.8485, 3.12, .8485> rotate y*45 translate <7.6, 0, 3.23> }
#if(mode)
  texture{
     pigment{color rgb 1.}
     finish{
        diffuse .9
     }
  }
#else
  texture{
     pigment{color rgb 0.}
     finish{
        ambient 0.0
        diffuse 1.0
     }
  }
#end
}

plane{
  #if(mode)
    <0,1,0>,0
  #else
    <0,1,0>,0.75
  #end
  #if(mode)
    texture{ pigment { color rgb .7 }}
  #else
    texture{
      pigment{color rgb 1.}
      finish{
        ambient 0.0
        diffuse 1.0
      }
    }
  #end
}

