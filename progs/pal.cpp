#include "pal.h"
#include <iostream>

void palette::map(double v, unsigned char* buf)
{
  double val = (v>=0.0 ? v/(max/6.) : 0.0);
  if(val<1.0) {
    buf[0] = 0;
    buf[1] = 0;
    buf[2] = (unsigned char)(val*255.);
  }
  else if(val<2.) {
    val -= 1.;
    buf[0] = (unsigned char)(val*255.);
    buf[1] = 0;
    buf[2] = 255;
  }
  else if(val<3.0) {
    val=3.-val;
    buf[0] = 255;
    buf[1] = 0;
    buf[2] = (unsigned char)(val*255.);
  }
  else if(val<5.0) {
    val-=3.0;
    buf[0] = 255;
    buf[1] = (unsigned char)(val*127.5);
    buf[2] = 0;
  }
  else if(val<6.0) {
    val-=5.0;
    buf[0] = 255;
    buf[1] = 255;
    buf[2] = (unsigned char)(val*255.);
  }
  else {
    buf[0] = 255;
    buf[1] = 255;
    buf[2] = 255;
  }
}


