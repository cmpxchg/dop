#include "mainwin.h"
#include <iostream>
#include <cmath>

void addr_cb(Fl_Widget *wid, void* dat)
{
  std::cout << "AddRec Callback called" << std::endl;
}

void dopon_cb(Fl_Widget *wid, void* dat)
{
  std::cout << "DOPOn Callback called" << std::endl;
  /*  if(dodop) {
    dodop=false;
  }
  else {
    dodop=true;
    pal.setMax(48.);
    legend->redraw();
    } */
}

void quit_cb(Fl_Widget *wid, void* dat)
{
  std::cout << "Quit Callback called" << std::endl;
  exit(0);
}


PalLeg::PalLeg(int x, int y, int w, int h, palette* pal, const char* t=0) : Fl_Box(x,y,w,h,t)
{
  p = pal;
}


void PalLeg::draw(void)
{
  uchar col[3];
  int i;
  int tlx = this->x();
  int tly = this->y()+fsi2;  // Down by half the font-height
  int wi = this->w();
  int he = this->h()-fsi;  // Reduced by one font-height
  int nlmax = floor(he/(3.*fsi));   // Divide by three times font-height
                                 // to get max nr of interm. labels
  //  std::cout << nlmax << std::endl;
  std::ostringstream vals;
  vals.precision(3);
  max = p->getMax();

  fl_color(0,0,0);
  fl_xyline(tlx+2.5*wi/5,tly, tlx+4*wi/5);
  vals << max;
  int wstr=0, hstr=0;
  fl_font(FL_HELVETICA, fsi);
  fl_measure(vals.str().c_str(), wstr, hstr);
  fl_draw(vals.str().c_str(), tlx+2.3*wi/5.-wstr, tly+0.3*hstr+1);
    //  std::cout << "Value: " << valstr << std::endl;
  bool lastpl=false;
  for(i=0; i<he-1; i++) {
    double val = i*max/(he-1);
    p->map(val, col);
    fl_color(col[0], col[1], col[2]);
    fl_xyline(tlx+3*wi/5,he-i-1+tly,tlx+4*wi/5);
    double tmp=i*nlmax/(double)he;
    if(fabs(tmp-round(tmp))<nlmax*.5001/he && !lastpl) {
      fl_color(0,0,0);
      fl_xyline(tlx+2.5*wi/5,he-i-1+tly, tlx+3*wi/5);
      vals.str(std::string());
      vals << val;
      int wstr=0, hstr=0;
      fl_font(FL_HELVETICA, fsi);
      fl_measure(vals.str().c_str(), wstr, hstr);
      fl_draw(vals.str().c_str(), tlx+2.3*wi/5.-wstr, he-i+0.3*hstr+tly);
	//        std::cout << "Value: " << valstr << std::endl;
      lastpl=true;
    }
    else {
      lastpl=false;
    }
  }
}


const char* butlab[] = { "Add Receiver", "Calculate DOP", "Quit", };

MainWindow::MainWindow(int w, int h, const char* t) : Fl_Double_Window(w,h,t)
{
  // 4:3 drawing are + 20% winwidth 
  // int fsi = (int)ceil(Fl::w()/64.);   // Font size
  std::cout << "Font size: " << fsi << std::endl;
  fl_font(FL_HELVETICA, fsi);
  //  pal.setMax(48.);
  dodop=false;
  int csize = 0;
  int hstr;
  for (unsigned int i=0; i<sizeof(butlab)/sizeof(const char*); i++) {
      int wstr = 0;
      hstr=0; 
      fl_measure(butlab[i], wstr, hstr);
      if(i==1)   // Add width of light for DOP-Label
	wstr+=(fsi+3);
      if(wstr>csize) {
	csize=wstr;
      }
  }
  csize = ceil(csize*1.1);
  hstr = ceil(hstr*1.25);
  std::cout << hstr << std::endl;
  canv = new PlotArea(0, 0, w-csize, h, &pal, 0);
  canv->end();
  canv->loadRecs("receivers.txt");
  this->resizable(canv);
  
  ctrls = new Fl_Group(w-csize,0,csize,h);
  
  addr = new Fl_Button(w-0.97*csize,.4*hstr,.94*csize,hstr,butlab[0]);
  addr->labelsize(fsi);
  addr->callback((Fl_Callback *)addr_cb, this);
  
  dopon = new Fl_Light_Button(w-0.97*csize,1.67*hstr,.94*csize,hstr,butlab[1]);
  dopon->labelsize(fsi);
  dopon->callback((Fl_Callback *)dopon_cb, this);

  legend = new PalLeg((int)(w-.97*csize),(int)(3.5*hstr),(int)(.94*csize),(int)(h-5.7*hstr), &pal);
  
  quit = new Fl_Button(w-.97*csize,h-1.25*hstr,.94*csize,hstr,butlab[2]);
  quit->labelsize(fsi);
  quit->callback((Fl_Callback *)quit_cb, this);

  ctrls->end();
  ctrls->resizable(legend);
}



