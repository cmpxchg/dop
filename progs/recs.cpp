#include "recs.h"


void reccoll::unmuteAll()
{
  for (std::map<std::string, receiver>::iterator it = recs.begin(); it != recs.end(); ++it)
  {
    it->second.unmute();
  }
}

std::vector<std::string> reccoll::listofrecs()
{
  std::vector<std::string> r;
  for(std::map<std::string, receiver>::iterator it=recs.begin(); it!=recs.end(); ++it) {
    r.push_back(it->first);
  }
  return(r);
}


void reccoll::parseline(std::string l, std::vector<std::string>& tokens)
{
  //  vector<string> tokens;
  boost::trim(l);
  boost::split(tokens, l, boost::is_any_of(" \t"), boost::token_compress_on);
  //  return(tokens);
}


int reccoll::readRecs(std::string fname)
{
  std::ifstream ifile(fname.c_str());
  std::string line;

  while(getline(ifile,line))
  {
    std::cout << line << std::endl;
    std::cout.flush();
    std::vector<std::string> toks;
    parseline(line, toks);

    if(toks.size()==6 && (toks.at(0)[0]!='#')) {
      std::string rn = toks.at(1);
      Eigen::Vector3d v(atof(toks.at(3).c_str()), atof(toks.at(4).c_str()),
		 atof(toks.at(5).c_str()));
      receiver r(rn, v);
      recs[rn]=r;
    }
  }
  nrec=recs.size();
  actrec=nrec;
  return nrec;
}

void reccoll::getBdBox(Eigen::Vector3d& min, Eigen::Vector3d& max)
{
  bool first=true;
  for (std::map<std::string, receiver>::iterator it = recs.begin(); it != recs.end(); ++it)
  {
    Eigen::Vector3d pos = it->second.getPos();
    for (int i=0; i<3; i++) {
      if(first || pos[i] < min[i])
        min[i] = pos[i];
      if(first || pos[i] > max[i])
        max[i] = pos[i];
    }
    if(first)
      first = false;
  }
}


std::string reccoll::findbyPos(Eigen::Vector2d where, double dist)
{
  for (std::map<std::string, receiver>::iterator it = recs.begin(); it != recs.end(); ++it)
  {
    Eigen::Vector3d pos = it->second.getPos();
    Eigen::Vector2d pos2 = Eigen::Vector2d(pos[0],pos[1]);
    if((pos2-where).norm()<dist) {
      return(it->first);
    }
  }  
  return(std::string(""));
}
