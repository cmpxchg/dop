#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Light_Button.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Box.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Button.H>
#include <sstream>
#include <cmath>
#include "plotarea.h"
#include "pal.h"

extern int fsi;
extern int fsi2;

class PalLeg : public Fl_Box {
 public:
  PalLeg(int x, int y, int w, int h, palette* p, const char* t);
  //  void map(double, unsigned char*);
 private:
  double max;
  palette* p;
  void draw(void);
};


class MainWindow : public Fl_Double_Window {
  public:
    MainWindow(int w, int h, const char* t);
  private:
    Fl_Group *ctrls;
    PlotArea *canv;
    Fl_Button *addr;
    PalLeg *legend;
    Fl_Button *quit;
    Fl_Light_Button *dopon;
    palette pal;
    bool dodop;
};


