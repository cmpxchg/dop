#ifndef PAL_H
#define PAL_H

class palette {
 public:
  palette(double m=1.0) { max=m; }
  void setMax(double m) { max=m; }
  double getMax(void) { return max; }
  void map(double, unsigned char*);
 private:
  double max;
};


#endif
