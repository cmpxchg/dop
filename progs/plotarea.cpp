#include "plotarea.h"
#include <iostream>


PlotArea::PlotArea(int x, int y, int w, int h, palette* p, const char* t=0) : Fl_Scroll(x,y,w,h,t)
{
  std::cout << "Plotarea constructor called" << std::endl;
  pal = p;
  draggin = false;
  needsrecalc = true;
  firstcalc = true;
  offsc = fl_create_offscreen(w,h);
  fl_register_images();
}

inline int abs(int v)
{
  return(v>0 ? v:-v);
}

int PlotArea::handle(int e)
{
  //  int ex, ey;
  Eigen::Vector2d metp;
  std::string recli;
  int shanres = Fl_Scroll::handle(e);
  switch(e){
    case FL_PUSH:
      pixpx = Fl::event_x();
      pixpy = Fl::event_y();
      pix2met(pixpx, pixpy, metp);
      recli = recs.findbyPos(metp, fsi2/scale);
      if(recli.size()) {
        std::cout << "Dragging receiver " << recli << std::endl;
	draggin = true;
	draggedr = recli;
      }
      //     std::cout << "Push at " << ex << ", " << ey << std::endl;
      return(1);
    case FL_RELEASE:
      std::cout << "Release" << std::endl;
      if(draggin) {
	pixpx = Fl::event_x();
	pixpy = Fl::event_y();
	// std::cout << "New position: " << pixpx << ", " << pixpy << std::endl;
        draggin = false;
	pix2met(pixpx, pixpy, metp);
	recs.setRecPos2(draggedr, metp);
	recs.setRecRecal(draggedr, true);
	recalc();
	this->redraw();
      }
      return(1);
    case FL_DRAG:
      if(draggin) {
	pixpx = Fl::event_x();
	pixpy = Fl::event_y();
	this->redraw();
      }
      //      std::cout << "Drag" << std::endl;
      return(1);
  }
  return(shanres);    
}


void PlotArea::loadRecs(std::string fname)
{
  Eigen::Vector3d min, max;
  recs.readRecs(fname);
  recs.getBdBox(min, max);
  xmin = min[0]-2.5;
  xmax = max[0]+2.5;
  ymin = min[1]-2.5;
  ymax = max[1]+2.5;
  double xsc = (xmax-xmin)/this->w();
  double ysc = (ymax-ymin)/this->h();
  if(xsc>ysc) {
    //    std::cout << "xsc: " << xsc << " yxc: " << ysc << std::endl;
    scale = 1./xsc;
    xoff = this->x();
    yoff = (this->h()-(ymax-ymin)*scale)/2+this->y();
  }
  else {
    scale = 1./ysc;
    xoff = (this->w()-(xmax-xmin)*scale)/2+this->x();
    yoff = this->y();
  }
}


void PlotArea::met2pix(Eigen::Vector3d met, int& px, int& py)
{
  px = (met[0]-xmin)*scale+xoff;
  py = this->h()+2*this->y()-(met[1]-ymin)*scale-yoff;
}


void PlotArea::pix2met(int px, int py, Eigen::Vector2d& met)
{
  met[0] = (px-xoff)/scale+xmin;
  met[1] = (this->h()+2*this->y()-py-yoff)/scale+ymin;
}


void PlotArea::recalc(void)
{
  fl_delete_offscreen(offsc);
  offsc = fl_create_offscreen(this->w(),this->h());
  fl_begin_offscreen(offsc);
  std::vector<std::string> rlist = recs.listofrecs();
  std::vector<Fl_Shared_Image*> ilist;
  std::vector<const char*> idata;
  raytadap rta;
  Eigen::Vector2d minv, maxv;
  pix2met(this->x(), this->y()+this->h(), minv);
  pix2met(this->x()+this->w(), this->y(), maxv);
  std::cout << "Min: " << minv << std::endl;
  std::cout << "Max: " << maxv << std::endl;
  for(std::vector<std::string>::iterator it=rlist.begin(); it!=rlist.end(); ++it) {
    if(recs.queryRecRecal(*it)) {
      rta.callRayt(*it, recs.getRecPos(*it), minv, maxv, this->w(), this->h());
      recs.setRecRecal(*it, false);
    }
    //    std::cout << *it << std::endl;
    //    std::cout << "X: " << this->x() << " ,Y: " << this->y() << std::endl;
    Fl_Shared_Image *img = Fl_Shared_Image::get((*it + ".png").c_str(),
						this->w(), this->h());
    if(firstcalc) {
      firstcalc = false;
    }
    else {
      img->reload();
    }
    idata.push_back(*(img->data()));
    ilist.push_back(img);
  }
  pal->setMax(ilist.size());
  for(int yp=0; yp<this->h(); yp++) {
    for(int xp=0; xp<this->w(); xp++) {
      int cnt = 0;
      for(unsigned int i=0; i<idata.size(); i++) {
        if((unsigned char)(idata[i][3*(yp*this->w()+xp)])>10)
	  cnt++;
      }
      unsigned char col[3];
      pal->map(cnt, col);
      fl_color(col[0],col[1],col[2]);
      fl_point(xp,yp);
    }
  }
  //  img->draw(0,0,this->w(), this->h(),0,0);
  for(std::vector<Fl_Shared_Image*>::iterator it=ilist.begin(); it!=ilist.end(); ++it) {
    (*it)->release();
  }
  fl_end_offscreen();
}


void PlotArea::draw(void)
{
  if(needsrecalc) {
    recalc();
    needsrecalc=false;
  }
  fl_copy_offscreen(this->x(), this->y(), this->w(), this->h(), offsc, 0, 0);
  //  img->scale(this->w(), this->h(),0);
  std::vector<std::string> rlist = recs.listofrecs();
  if(!rlist.empty()) {
    fl_color(255,255,255);
    for(std::vector<std::string>::iterator it=rlist.begin(); it!=rlist.end(); ++it)
    {
      int x,y;
      Eigen::Vector3d rp = recs.getRecPos(*it);
      if(draggin && *it==draggedr) {
	x = pixpx;
	y = pixpy;
      }
      else {
        met2pix(rp, x, y);
      }
      fl_xyline(x-fsi2,y,x+fsi2);
      fl_yxline(x,y-fsi2,y+fsi2);
      fl_arc(x-fsi2/2,y-fsi2/2,fsi2+1,fsi2+1,0.0,360.0);

      std::ostringstream he;
      he.precision(3);
      he << rp[2];
      fl_draw(he.str().c_str(), x-fsi, y-3*fsi/4);
      fl_draw(it->c_str(), x+fsi2, y+fsi);
    }
    
  }
  else
    std::cout << "empty receiver-list" << std::endl;
}

void PlotArea::resize(int x, int y, int w, int h)
{
  Fl_Scroll::resize(x,y,w,h);
  needsrecalc=true;
  double xsc = (xmax-xmin)/this->w();
  double ysc = (ymax-ymin)/this->h();
  if(xsc>ysc) {
    scale = 1./xsc;
    xoff = x;
    yoff = (this->h()-(ymax-ymin)*scale)/2+y;
  }
  else {
    scale = 1./ysc;
    xoff = (this->w()-(xmax-xmin)*scale)/2+x;
    yoff = y;
  }
  std::cout << "Resize called" << std::endl;
}
