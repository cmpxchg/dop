#ifndef RECS_H
#define RECS_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <functional>
#include <boost/algorithm/string.hpp>

#ifdef Success
  #undef Success
#endif
#include <Eigen/Dense>

class receiver
{
  std::string name;
  Eigen::Vector3d pos;
  bool rmute;
  bool needsrec;
public:
 receiver(std::string s="", Eigen::Vector3d p=Eigen::Vector3d(0.,0.,0.)) : name(s), pos(p) { rmute=false; needsrec=true; }
  void setName(std::string s) { name=s; }
  std::string getName() { return name; }
  void setPos2(Eigen::Vector2d p) { pos[0]=p[0]; pos[1]=p[1]; }
  void mute() { rmute=true; }
  void unmute() { rmute=false; }
  void setrecal(bool v) { needsrec=v; }
  bool queryrecal() { return needsrec; }
  bool querymute() { return rmute; }
  Eigen::Vector3d getPos() { return(pos); }  
  void print() { std::cout << name << std::endl << pos << std::endl << "****" << std::endl; }
};


class reccoll
{
private:
  std::map<std::string, receiver> recs;
  void parseline(std::string, std::vector<std::string>&);
  int nrec;
  int actrec;
public:
  reccoll() { nrec=0; actrec=0; }
  void addRec(receiver newr) { recs[newr.getName()]=newr; nrec++; actrec++; }
  void delRec(std::string s) { recs.erase(s); nrec--; actrec--; }
  void muteRec(std::string s) {recs[s].mute(); actrec--; }
  void unmuteAll();
  int readRecs(std::string);
  void getBdBox(Eigen::Vector3d&, Eigen::Vector3d&);
  Eigen::Vector3d getRecPos(std::string s) { return recs[s].getPos(); }
  void setRecPos2(std::string s, Eigen::Vector2d p) { recs[s].setPos2(p); std::cout << "Set Pos for " << s << "to " << p <<std::endl;}
  void setRecRecal(std::string s, bool v) { recs[s].setrecal(v); }
  bool queryRecRecal(std::string s) { return recs[s].queryrecal(); }
  std::string findbyPos(Eigen::Vector2d, double dist);
  std::vector<std::string> listofrecs();
  Eigen::Matrix2d DOPmat(Eigen::Vector3d);
  double DOP(Eigen::Vector3d);
};


#endif
