#include <FL/Fl.H>
#include "mainwin.h"

int fsi;
int fsi2;

int main(int argc, char **argv) {
  // Window at 70% screenwidth, 5:3 aspect ratio (20% width for controls)
  int wi = (int)(Fl::w()*.7);
  fsi = (int)ceil(Fl::h()/36.);
  //  fsi = 36;
  fsi2 = fsi/2;
  Fl_Window* mw = new MainWindow(wi, (int)(.6*wi), "Ausleuchtungssimulation");
  mw->show(argc, argv);
  return Fl::run();
}
