#include "raytadap.h"


void raytadap::callRayt(std::string rname, Eigen::Vector3d pos, Eigen::Vector2d min, Eigen::Vector2d max, int w, int h)
{
  std::stringstream callstr;
  callstr << "povray +Imain.pov -d +FN +O" << rname << ".png +W" << w << " +H" << h;
  std::cout << "Rendering illum area for receiver " << rname << std::endl;
  std::cout << "Min/max: " << min << std::endl << max << std::endl;

  std::fstream cam;
  Eigen::Vector2d loc = 0.5*(min+max);
  double right = (max[0]-min[0])/2.0;
  double up = (max[1]-min[1])/2.0;
  cam.open("camera.inc", std::fstream::out);
  cam << "camera {" << std::endl << "    orthographic" << std::endl;
  cam << "    location <" << loc[0] << ", 2.5, " << loc[1] << ">" << std::endl;
  cam << "    sky z*1.0" << std::endl;
  cam << "    up z*" << 2.*up << std::endl;
  //  cam << "    right x*" << 2.*right*w/h << std::endl;
  cam << "    right x*" << 2.*right << std::endl;
  cam << "    direction y*-1.0" << std::endl << "}" << std::endl;
  cam.close(); 
  std::fstream slight;
  slight.open("spotlights.inc", std::fstream::out);
  slight << "light_source {" << std::endl;
  slight << "    <" << pos[0] << ", " << pos[2] << ", " << pos[1] << ">" << std::endl;
  slight << "    color rgb 1.0" << std::endl << "    spotlight" << std::endl;
  slight << "    point_at <" << pos[0] << ", 0.0, " << pos[1] << ">" << std::endl;
  slight << "    radius micang" << std::endl << "    falloff micang" << std::endl;
  slight << "}" << std::endl;
  slight.close();
  std::system(callstr.str().c_str());
  /*  conf << "Receiver " << rname << std::endl;
  conf << "Pos: " << pos << std::endl;
  conf << "Right: " << (max[0]-min[0])/2.0 << std::endl;
  conf << "Up: " << (max[1]-min[1])/2.0 << std::endl;
  conf << "Pos: " << 0.5*(min+max) << std::endl;
  conf << "Resol: " << w << " x " << h << std::endl;
  conf.close(); */
}
