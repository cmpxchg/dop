\documentclass{scrartcl}
\usepackage[T1]{fontenc}
\usepackage{lmodern, microtype}
\usepackage{amsmath, amsfonts}
\newcommand{\R}{\mathbb{R}}

\title{DOP for TDOA localization in 2 and 3 dimensions}
\author{Jan Schlemmer}

\begin{document}
\maketitle
\section{Preliminaries}
\subsection{Normally distributed random variables and linear transforms}
The standing assumption will be that measurements of arrival times on 
receivers will be independently and identically distributed according to a 
normal distribution with standard derivation $\sigma$.

Consider for the moment the more general situation of $n$ random variables 
$X_1, \ldots, X_n$ on $\R^n$ with probability density
\begin{displaymath}
\frac{1}{\mathcal{N}} \exp(-x^T A^{-1} x) \,,
\end{displaymath}
$A$ a positive definite $n \times n$ matrix. The corresponding covariance 
matrix is then given by
\begin{align*}
\mathrm{Cov}_{ij}
  = &
\frac{1}{\mathcal{N}} \int_{\R^n} \exp(-x^T A^{-1} x) \left(x x^T\right)_{ij}
d^n x \\
  = &
\frac{\sqrt{\det A^{1/2}}}{\mathcal{N}} 
  \int_{\R^n} \exp(-y^T y) \left(A^{1/2} y y^T A^{1/2}\right)_{ij} d^n y \\
  = & \left[
A^{1/2} \ \frac{\sqrt{\det A^{1/2}}}{\mathcal{N}}
  \int_{\R^n} \exp(-y^T y) \ y y^T  d^n y \ A^{1/2} \right]_{ij}\\ 
  = &
\left[A^{1/2} \, \mathbb{1} A^{1/2}\right]_{ij} \\
  = & 
A_{ij}
\end{align*}
i.e. $A$ \emph{is} the covariance matrix for these random variables.

The moment-generating function for these random variables is
\begin{align*}
 M_X(p) 
  := & 
 \mathbb{E}\left[\exp(i p \cdot x)\right] \\
   = &
\frac{1}{\mathcal{N}} \int_{\R^n} \exp(-x^T A^{-1} x + i p \cdot x) d^n x \\
   = & 
\frac{1}{\mathcal{N}} \int_{\R^n} \exp\left[-\left(x-\tfrac{i}{2} p\right)^T
       A^{-1} \left(-\left(x - \tfrac{i}{2} p \right)\right) \right]
       \exp\left(-\frac{p^T A p}{4}\right) d^n x \\
   = &
\exp\left(-\frac{p^T A p}{4} \right) \,,
\end{align*}
so $A$ also appears in $M_X(p)$.

If we now consider $m$ random variables $Z$ related to $X$ by a linear
transform $B$, i.e. $Z = B X$ (potentially $m<n$, i.e. $B$ not
rectangular), we get for their moment-generating function $M_Z$:
\begin{align*}
  M_Z(q)
   =&
  \mathbb{E}\left[\exp (i q \cdot B x)\right] \\
   =& M_X(B^T q) = \exp\left(-\frac{q^T B A B^T q}{4}\right) \,.
\end{align*}
This however means, that the random variables $Z$ is again normally distributed
with covariance $C_Z = B A B^T$ whereas the matrix appearing in the probability
density when realizing the $Z_k$ directly on $\R^m$ is given as
\begin{displaymath}
  (B A B^T)^{-1} = C_Z^{-1} \,.
\end{displaymath}


\subsection{Application: Distribution of distance-differences}
Consider the $n$ independently normal-distributed random variables $s_1$, 
\ldots, $s_n$ with identical standard-derivation $\sigma$. Now form differences 
\begin{displaymath}
  s_2' = s_2 - s_1, \ldots, s_n' = s_n - s_1 \,.
\end{displaymath}
These are related to the $s_1$, \ldots, $s_n$ via the linear transformation
\begin{displaymath}
\begin{pmatrix} s_2' \\ s_3' \\ \vdots s_n' \end{pmatrix}
  = 
\begin{pmatrix}
-1 & 1 & 0 & 0 & \ldots & 0 & 0 \\
-1 & 0 & 1 & 0 & \ldots & 0 & 0 \\
\vdots & \vdots & & & & \vdots & \\
-1 & 0 & 0 & 0 & \ldots & 0 & 1 
\end{pmatrix}
\begin{pmatrix}
  s_1 \\ s_2 \\ \vdots \\ s_n
\end{pmatrix}
  =:
S s
\end{displaymath}
Taking $(s_1, \ldots, s_n)$ as $(X_1, \ldots, X_n)$ and $(s_2', \ldots, s_n')$ as
$(Y_1, \ldots, Y_n)$, the covariance for the $(s_2', \ldots, s_n')$ is obtained
with the formula from last section as
\begin{displaymath}
\mathrm{Cov}
  =
\sigma^2 S S^T
  = 
\sigma^2
\begin{pmatrix}
2 & 1 & 1& \ldots & 1 \\
1 & 2 & 1 & \ldots & 1 \\
\vdots & & & \\
1 & \ldots & 1 & 1 & 2
\end{pmatrix} =: \sigma^2 Q
\end{displaymath}

\section{DOP}
\subsection{3 dimensions}
Assume our sender is located at a point $r \in \R^3$ and we have receivers at positions
$q_1, \ldots, q_n$, $q_i \in \R^3$ for $i \in \{1, \ldots, n\}$. For TDOA location finding we are not
directly given the distances $s_i := \lVert r-q_i \rVert$ but rather their difference to one of them
(here taken wlog. as the first receiver at $q_1$), i.e. ${s_2}' = s_2 - s_1$, ${s_3}' = s_3 - s_1$,
\ldots, ${s_n}' = s_n - s_1$.

When estimating the position, we thus also need to determine $s:=s_1$, so we need to minimize
\begin{equation}
  E_{q_1, \ldots, q_n, {s_2}', \ldots, {s_n}'}(r, s)
  =
  (\lVert q_1 - r \rVert - s)^2 + (\lVert q_2 - r \rVert - {s_2}'-s)^2 +
  \ldots +
  (\lVert q_n - r \rVert - {s_n}' - s)^2
  \label{eqn_errorfun}
\end{equation}
with respect to $r \in \R^3$ and $s \in \R^+$. The necessary condition then leads to
\begin{align}
  \sum_{k=1}^n r-q_k
    =&
  s \: \widehat{r-q_1} + \sum_{k=2}^n (s+{s_k}') \: \widehat{r-q_k}
  \label{eqn_rcond} \\
  s + \sum_{k=2}^n (s+{s_k}')
    =&
  \sum_{k=1}^n \rVert r - q_k \rVert \label{eqn_scond},
\end{align}
with
\begin{displaymath}
  \hat{v} := \frac{1}{\lVert v \rVert} \: v \qquad \text{ for } v \in \R^3 \,,
\end{displaymath}
which are clearly satisfied for $s = \lVert r-q_1 \rVert$, ${s_2}' = \lVert r-q_2 \rVert - s$, \ldots,
${s_n}' = \lVert r - q_n \rVert - s$

From \eqref{eqn_scond} we can determine $s$ as
\begin{displaymath}
  s = \frac{1}{n} \left( \sum_{k=1}^n \lVert r- q_k \rVert - \sum_{k=2}^n {s_k}' \right) \,.
\end{displaymath}
Inserting this back into \eqref{eqn_rcond}, we get the determining relation for $r$
\begin{equation}
  \sum_{k=1}^n r-q_k
  =
  \frac{1}{n} \left(  \sum_{k=1}^n \lVert r- q_k \rVert - \sum_{k=2}^n {s_k}' \right)\:
  \left( \sum_{l=1}^n \widehat{r-q_l} \right)
  + \sum_{k=2}^n {s_k}' \: \widehat{r-q_k} \,
  \label{eqn_masterrel}
\end{equation}
which -- in addion to $r$ -- only contains the known quantities $q_1, \ldots, q_n$
(the receiver positions) and the measured quantities ${s_2}', \ldots, {s_n}'$ and
thus can be used to determine the position $r$ of the sender.

To perform error analysis, we start at a position $r_0$ and perturb the
(correct) distance differences
\begin{align*}
  {\overset{o}{s_2}}'
  =&
     \lVert r_0 - q_2 \rVert - \lVert r_0 - q_1 \rVert \\
  \vdots & \\
  {\overset{o}{s_n}}'
  =&
  \lVert r_0 - q_n \rVert - \lVert r_0 - q_1 \rVert \\           
\end{align*}
corresponding to this position to new ones
\begin{align*}
  {s_2}'
  =&
     {\overset{o}{s_2}}' + \delta s_2 - \delta s_1 \\
     \vdots & \\
  {s_n}'
  =&
     {\overset{o}{s_n}}' + \delta s_n - \delta s_1   \,.     
\end{align*}
Then we use the master-relation \eqref{eqn_masterrel} to determine a new position
\begin{displaymath}
  r = r_0 + \delta r 
\end{displaymath}
from the these modified ${s_k}'$. As we are interested in the behaviour for small errors $\delta s_k$,
\eqref{eqn_masterrel} will however be equated to \emph{liner order in the $q_k$ and $\delta r$}. \\
To do this, the following relations are helpful:
\begin{align*}
  \lVert r - q_k \rVert
  =&
     \lVert r_0 - q_k \rVert + \widehat{r - q_k} \cdot \delta r + \ldots \\
  \widehat{r-q_k}
  =&
     \widehat{r_0 - q_k} + \frac{\delta r}{\lVert r_0 - q_k \rVert}
     -\underbrace{\left( \widehat{r_0-q_k} \cdot \frac{\delta r}{\lVert r_0 - q_k \rVert}\right) \:
     \widehat{r_0 - q_k}}_{=:E_{\widehat{r_0-q_k}}\left(\frac{\delta r}{\lVert r_0 - q_k \rVert}\right)} + \ldots \,.
\end{align*}
Here $a \cdot b$ for $a \in \R^3, b \in \R^3$ denotes their scalar product, the
component of a vector $v \in \R^3$ along a unit-vector $\hat{w} \in \R^3$ is
abbreviated as $E_{\hat{w}}(v)$, i.e.
$E_{\hat{w}}(v) = (v \cdot \hat{w}) \hat{w}$.

Using these relations, we calculate for the terms in equation
\eqref{eqn_masterrel}
\begin{align*}
  \sum_{k=1}^n r - q_k
  =&
     \sum_{k=1}^n r_0 - q_k \, + n \, \delta r + \ldots \\
  \sum_{k=1}^n \lVert r-q_k \rVert
  =&
  \sum_{k=1}^n \lVert r_0-q_k \rVert
  + \sum_{k=1}^n \widehat{r_0-q_k} \, \delta r +\ldots \\
  - \sum_{k=2}^n {s_k}'
  =&
  - \sum_{k=2}^n {\overset{o}{s_k}}' - \sum_{k=2}^n \delta s_k
  + (n-1) \delta s_1 + \ldots \\
  \sum_{l=1}^n \widehat{r-q_l}
  =&
  \sum_{l=1}^n \widehat{r_0-q_l}
  + \sum_{l=1}^n \frac{1}{\lVert r_0 - q_l \rVert} \ \delta r
  - \sum_{l=1}^n E_{\widehat{r_0-q_l}}\left(
              \frac{\delta r}{\lVert r_0 - q_l \rVert}\right) + \ldots
\end{align*}
and thus, using the definition of the ${\overset{o}s_k}'$:
\begin{multline*}
  \frac{1}{n} \left( \sum_{k=1}^n \lVert r - q_k \rVert
    - \sum_{k=2}^n {s_k}' \right) \left( \sum_{l=1}^n \widehat{r-q_l} \right)
    =
    \lVert r_0 - q_1  \rVert \sum_{l=1}^n \widehat{r_0-q_l} \\
    + \delta r \sum_{l=1}^n \frac{\lVert r_0 - q_1 \rVert}
                                {\lVert r_0 - q_l \rVert} 
    - \lVert r_0 - q_1 \rVert \sum_{l=1}^n 
           E_{\widehat{r-q_l}} \left(\frac{\delta r}{\lVert r_0 - q_l \rVert} 
                            \right) \\
    + \frac{1}{n} \sum_{k=1}^n \sum_{l=1}^n (\widehat{r_0 - q_k} \cdot \delta r)
                                           \widehat{r_0 - q_l}
    + \frac{(n-1) \delta s_1 - \sum_{k=2}^n \delta s_k}{n}
      \sum_{l=1}^n \widehat{r_0 - q_l}
\end{multline*}
as well as
\begin{multline*}
  \sum_{k=2}^n s_k' \widehat{r-q_k}
  =
  \sum_{k=2}^n r_0 - q_k - \lVert r_0
  - q_1 \rVert \sum_{k=2}^n \widehat{r_0-q_k} \\
  + (n-1) \delta r
  - \delta r \sum_{k=2}^n \frac{\lVert r_0 - q_1 \rVert}
                              {\lVert r_0 - q_k \rVert} \\
  + \lVert r_0 - q_1 \rVert \sum_{k=2}^n
     E_{\widehat{r_0 - q_k}}\left( \frac{\delta r}{\lVert r_0 - q_k \rVert}\right)
  - \sum_{k=2}^n E_{\widehat{r_0 - q_k}}( \delta r ) + \ldots \,.
\end{multline*}
For the lhs of \eqref{eqn_masterrel} on the other hand we have:
\begin{displaymath}
  \sum_{k=1}^n  r - q_k = n \delta r + \sum_{k=1}^n r_0 - q_k \,.
\end{displaymath}
Thus, the zero-order terms on the left and right hand side in
\eqref{eqn_masterrel} cancel out (as they have to), leaving the linear order
equation
\begin{align*}
  n \delta r
  =&
  n \delta r - \sum_{k=1}^n E_{\widehat{r_0 - q_k}}(\delta r)
  + \frac{\sum_{k=1}^n \widehat{r_0 - q_k} \cdot \delta r
    + (n-1) \delta s_1 - \sum_{k=2}^n \delta s_k}{n}
  \sum_{l=1}^n \widehat{r_0 - q_l} + \ldots \,,
\end{align*}
or bringing $\delta r$ to one and the $\delta s$s to the other side:
\begin{align}
 \sum_{k=1}^n E_{\widehat{r_0 - q_k}}(\delta r)
 - \frac{\sum_{k=1}^n \widehat{r_0 - q_k} \cdot \delta r}{n}
   \sum_{l=1}^n \widehat{r_0 - q_l}
   =&
   \frac{(n-1) \delta s_1 - \sum_{k=2}^n \delta s_k}{n}
   \sum_{l=1}^n \widehat{r_0 - q_l}
   \label{eqn_linrelinterm}
\end{align}
Introducing the vector $w_{r_0, q_1, \ldots, q_n}$ as
\begin{equation}
  w_{r_0, q_1, \ldots, q_n} := \sum_{l=1}^n \widehat{r_0 - q_l}
\end{equation}
and the linear operator
\begin{align}
  F_{r_0, q_1, \ldots, q_n}:
 v \mapsto 
 (\sum_{k=1}^n \widehat{r_0 - q_k} \cdot v)
   w_{r_0, q_1, \ldots, q_n}
 - n \sum_{k=1}^n E_{\widehat{r_0 - q_k}}(v)
\end{align}
and denoting $\delta s_k - \delta s_1$ by $\delta(s_k')$, equation
\eqref{eqn_linrelinterm} can also be written as
\begin{align}
  F_{r_0, q_1, \ldots, q_n}(\delta r)
  =& \sum_{k=2}^n \delta(s_k') w_{r_0, q_1, \ldots, q_n}
\end{align}

\subsection{(Pseudo) 2 dimensional}
Next, we consider the situation, where the height (i.e. the $z$-coordinate of $r$) is not determined
during determination of the location, but assumed as some known $h$. When estimating the position, we
thus do not minimize \eqref{eqn_errorfun} over $s \in \R^+$ and $r \in \R^3$ but only over
$s \in \R^+$ and $r \in \{(x^{(1)}, x^{(2)}, x^{(3)}) \in \R^3 \mid x^{(3)} = h\} =: S_h$, i.e. $r$
is constrained to move in the plane $S_h$ parallel to the ground at height $h$. This however implies,
that only the first two components of equation \eqref{eqn_rcond} remain, which in turn implies, that
also the master-relation \eqref{eqn_masterrel} is reduced to its first two components.

Again we perturb the distance-differences around their values in a position $r_0$ as before, we
do however only allow $r$ to differ from $r_0$ by a vector $\delta r_2$ with third component
equal to zero.
\end{document}

